package examen.parcial.pkg1;

/**
 *
 * @author ameri
 */
public class EXAMENPARCIAL1 {

    public static void main(String[] args) {
        int arreglo[] = {4, 8, 5, 2, 9, 10, 14, 1, 11};

        int i;

        System.out.println("Arreglo original:");

        for (i = 0; i < arreglo.length; i++) {

            System.out.print(" " + arreglo[i]);

        }

        for (i = arreglo.length; i > 1; i--) {

            ordenar(arreglo, i - 1);

        }

        System.out.println("\nArreglo Ordenado: ");

        for (i = 0; i < arreglo.length; i++) {

            System.out.print(" " + arreglo[i]);

        }
    }

    public static void ordenar(int array[], int arr_or) {

        int i, o;
        int A, B, C, r, t;

        r = (arr_or - 1) / 2;

        for (o = r; o >= 0; o--) {

            for (i = r; i >= 0; i--) {

                A = (2 * i) + 1;
                B = (2 * i) + 2;

                if ((A <= arr_or) && (B <= arr_or)) {

                    if (array[B] >= array[A]) {
                        C = B;
                    } else {
                        C = A;
                    }

                } else {

                    if (B > arr_or) {
                        C = A;
                    } else {
                        C = B;
                    }

                }

                if (array[i] < array[C]) {

                    t = array[i];
                    array[i] = array[C];
                    array[C] = t;

                }
            }
        }

        t = array[0];
        array[0] = array[arr_or];
        array[arr_or] = t;
        return;

    }

}
